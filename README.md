MNkmrRegistry
=============

A personal julia package registry


## Adding registry

Open julia REPL and press <kbd>]</kbd> to get into Pkg REPL mode.

```
pkg> registry add https://gitlab.com/mnkmr/MNkmrRegistry
```


## Updating registry

Open julia REPL and press <kbd>]</kbd> to get into Pkg REPL mode.

```
pkg> registry up MNkmrRegistry
```


## Removing registry

Open julia REPL and press <kbd>]</kbd> to get into Pkg REPL mode.

```
pkg> registry rm MNkmrRegistry
```
